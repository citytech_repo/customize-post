(function($){
	'use strict'
	$(".fancybox").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
	$(".video-light-box").fancybox({ 
		openEffect  : 'none',
		closeEffect : 'none',
		helpers: {
			media: {}
		}
	}); 
	setTimeout(function() {
		$('.video-light-box').each(function(index, element) { 
			var yutube_url=$(element).attr('href');
			var youtube_id=yutube_url.split('https://www.youtube.com/watch?v=');
			$(element).find('img').attr('src','http://img.youtube.com/vi/'+youtube_id[1]+'/0.jpg');
		});
	}, 800);
})(jQuery)

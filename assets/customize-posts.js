(function($){ 
	'use strict'
	$(document).ready(function() { 

		$('.custom_file_metabox').each(function(index, el) {

			$(el).find('.upload_file').on('click',function(index){

				var send_attachment_bkp = wp.media.editor.send.attachment; 

				wp.media.editor.send.attachment = function(props, attachment) {  

					$(el).find('.meta_box_file').attr('value', attachment.url);

					wp.media.editor.send.attachment = send_attachment_bkp; 
					console.log(attachment);
				}

				wp.media.editor.open(); 

				return false;
			})
			$(el).find('.upload_file_clear').on('click',function(event){

				event.preventDefault();

				$(el).find('.meta_box_file').attr('value', '');  

			})

			//$('#custom_meta_field').scrollTop($('#custom_meta_field').height())
		});

	});
})(jQuery);

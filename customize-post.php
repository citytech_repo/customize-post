<?php
/**
 * Plugin Name:     Customize Post
 * Plugin URI:      http://citytechcorp.com
 * Description:     A handy way to manage your post types, taxonomies and there ordering listing. Awesome! Is't it?

 * Author:          Citytech
 * Author URI:      http://citytechcorp.com
 * Version:         1.1.0
 * Text Domain: Optional. Plugin's text domain for localization. Example: mytextdomain
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: A short license name. Example: GPL2
 * @package         Customize_Post
 */
if (!defined('ABSPATH')) {
	exit;
}
define('CPT_VERSION', '1.1.0'); // Left for legacy purposes.
define('customizeposts_VERSION', '1.1.0');
define('customizeposts_WP_VERSION', get_bloginfo('version'));
/* For
 * @Meta Boxes
 */
global $wpdb;
global $table_metabox_fields;
global $table_metabox_metabox;
global $table_metabox_fieldtype;
global $table_postmeta;
$table_metabox_fields = $wpdb->prefix . "metabox_fields";
$table_metabox_metabox = $wpdb->prefix . "metabox_metabox";
$table_metabox_fieldtype = $wpdb->prefix . "metabox_fieldtype";
$table_postmeta = $wpdb->prefix . "postmeta";
//This are the common files which are included for Global Use
include 'common.php';
include 'function.php';
register_activation_hook(__FILE__, 'wcmb_metabox_install');
register_deactivation_hook(__FILE__, 'wcmb_metabox_uninstall');
/*
 *
 * @ End of Metabox Initialize Code
 *
 */
/**
 * Load our Admin UI class that powers our form inputs.
 *
 * @sind 1.0.0
 *
 * @internal
 */
function customizeposts_load_ui_class() {
	require_once plugin_dir_path(__FILE__) . 'classes/class.customizeposts_admin_ui.php';
	require_once plugin_dir_path(__FILE__) . 'classes/class.customizeposts_debug_info.php';
	require_once plugin_dir_path(__FILE__) . 'classes/class.customizeposts_post_order.php';
	require_once plugin_dir_path(__FILE__) . 'admin/shortcode.customizeposts.php';
	require_once plugin_dir_path(__FILE__) . 'admin/shortcode.submenu.php';
}
add_action('init', 'customizeposts_load_ui_class');
/**
 * Set a transient used for redirection upon activation.
 *
 * @since 1.0.0
 */
function customizeposts_activation_redirect() {
	// Bail if activating from network, or bulk.
	if (is_network_admin()) {
		return;
	}
	// Add the transient to redirect.
	set_transient('customizeposts_activation_redirect', true, 30);
}
add_action('activate_' . plugin_basename(__FILE__), 'customizeposts_activation_redirect');
/**
 * Redirect user to customizeposts about page upon plugin activation.
 *
 * @since 1.0.0
 */
function customizeposts_make_activation_redirect() {
	if (!get_transient('customizeposts_activation_redirect')) {
		return;
	}
	delete_transient('customizeposts_activation_redirect');
	// Bail if activating from network, or bulk.
	if (is_network_admin()) {
		return;
	}
	$query_args = array(
		'page' => 'customizeposts_manage_post_types',
	);
	// Redirect to customizeposts about page.
	wp_safe_redirect(add_query_arg($query_args, customizeposts_admin_url('admin.php?page=customizeposts_manage_post_types')));
}
add_action('admin_init', 'customizeposts_make_activation_redirect', 1);
/**
 * Flush our rewrite rules on deactivation.
 *
 * @since 0.8.0
 *
 * @internal
 */
function customizeposts_deactivation() {
	flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'customizeposts_deactivation');
/**
 * Register our text domain.
 *
 * @since 0.8.0
 *
 * @internal
 */
function customizeposts_load_textdomain() {
	load_plugin_textdomain('customizeposts');
}
add_action('plugins_loaded', 'customizeposts_load_textdomain');
/**
 * Load our main menu.
 *
 * Submenu items added in version 1.1.0
 *
 * @since 0.1.0
 *
 * @internal
 */
function customizeposts_plugin_menu() {
	/**
	 * Filters the required capability to manage customizeposts settings.
	 *
	 * @since 1.3.0
	 *
	 * @param string $value Capability required.
	 */
	$capability = apply_filters('customizeposts_required_capabilities', 'manage_options');
	$parent_slug = 'customizeposts_main_menu';
	add_menu_page(__('Custom Post Types', 'customizeposts'), __('CUSTOMIZE POST', 'customizeposts'), $capability, $parent_slug, 'customizeposts_settings', customizeposts_menu_icon());
	add_submenu_page($parent_slug, __('Add/Edit Post Types', 'customizeposts'), __('Add/Edit Post Types', 'customizeposts'), $capability, 'customizeposts_manage_post_types', 'customizeposts_manage_post_types');
	add_submenu_page($parent_slug, __('Add/Edit Taxonomies', 'customizeposts'), __('Add/Edit Taxonomies', 'customizeposts'), $capability, 'customizeposts_manage_taxonomies', 'customizeposts_manage_taxonomies');
	add_submenu_page($parent_slug, __('Registered Types and Taxes', 'customizeposts'), __('Registered Types/Taxes', 'customizeposts'), $capability, 'customizeposts_listings', 'customizeposts_listings');
	add_submenu_page($parent_slug, __('Order/Reorder', 'customizeposts'), __('Order/Reorder', 'customizeposts'), $capability, 'customizeposts_orderreorder', 'customizeposts_orderreorder');
	add_submenu_page($parent_slug, __('Meta Boxes', 'customizeposts'), __('Meta Boxes', 'customizeposts'), 'manage_options', 'metabox-listing', 'wcmb_metabox_listing');
	add_submenu_page($parent_slug, __('Frontend Shortcodes', 'customizeposts'), __('Frontend Shortcodes', 'customizeposts'), 'manage_options', 'shortcode-listing', 'shortcode_listing');
	/**
	 * Fires after the default submenu pages.
	 *
	 * @since 1.1.0
	 *
	 * @param string $value      Parent slug for Customize Post menu.
	 * @param string $capability Capability required to manage customizeposts settings.
	 */
	do_action('customizeposts_extra_menu_items', $parent_slug, $capability);
	// Remove the default one so we can add our customized version.
	remove_submenu_page($parent_slug, 'customizeposts_main_menu');
}
add_action('admin_menu', 'customizeposts_plugin_menu');
/**
 * Fire our customizeposts Loaded hook.
 *
 * @since 1.3.0
 *
 * @internal Use `customizeposts_loaded` hook.
 */
function customizeposts_loaded() {
	/**
	 * Fires upon plugins_loaded WordPress hook.
	 *
	 * customizeposts loads its required files on this hook.
	 *
	 * @since 1.3.0
	 */
	do_action('customizeposts_loaded');
}
add_action('plugins_loaded', 'customizeposts_loaded');
/**
 * Load our submenus.
 *
 * @since 1.0.0
 *
 * @internal
 */
function customizeposts_create_submenus() {
	require_once plugin_dir_path(__FILE__) . 'inc/utility.php';
	require_once plugin_dir_path(__FILE__) . 'inc/post-types.php';
	require_once plugin_dir_path(__FILE__) . 'inc/taxonomies.php';
	require_once plugin_dir_path(__FILE__) . 'inc/listings.php';
	require_once plugin_dir_path(__FILE__) . 'inc/order_reorder.php';
}
add_action('customizeposts_loaded', 'customizeposts_create_submenus');
/**
 * Fire our customizeposts init hook.
 *
 * @since 1.3.0
 *
 * @internal Use `customizeposts_init` hook.
 */
function customizeposts_init() {
	/**
	 * Fires upon init WordPress hook.
	 *
	 * @since 1.3.0
	 */
	do_action('customizeposts_init');
}
add_action('init', 'customizeposts_init');
/**
 * Enqueue customizeposts admin styles.
 *
 * @since 1.0.0
 *
 * @internal
 */
/*if (function_exists('wp_enqueue_media')) {
wp_enqueue_media();
} else {
wp_enqueue_style('thickbox');
wp_enqueue_script('media-upload');
wp_enqueue_script('thickbox');
}*/
function customizeposts_add_styles() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
	if (defined('DOING_AJAX') && DOING_AJAX) {
		return;
	}
	$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
	wp_enqueue_script('customizeposts', plugins_url("js/customizeposts.min.js", __FILE__), array(
		'jquery',
		'postbox',
	), customizeposts_VERSION, true);
	wp_enqueue_style('customizeposts-css', plugins_url("css/customizeposts{$min}.css", __FILE__), array(), customizeposts_VERSION);

	wp_enqueue_script(
		'customize-posts-js',
		plugins_url('assets/customize-posts.js', __FILE__),
		array('jquery')
	);

}
add_action('admin_enqueue_scripts', 'customizeposts_add_styles');

function my_scripts_method() {
	wp_enqueue_script('jquery-js', plugins_url('assets/jquery.js', __FILE__), array('jquery'));
	wp_enqueue_style('customize-posts-css', plugins_url('assets/customize-posts.css', __FILE__), array());
	/**
	 * @Bootstrap script loading
	 * @Customize Post
	 **/
	if (get_option('bs_mode') == 'yes') {
		wp_enqueue_script('bootstrap-js', plugins_url('assets/bootstrap/js/bootstrap.min.js', __FILE__), array('jquery'));
		wp_enqueue_style('bootstrap-css', plugins_url('assets/bootstrap/css/bootstrap.css', __FILE__), array());
	}
	/**
	 * @Fancybox script loading
	 * @Customize Post
	 **/
	if (get_option('fb_mode') == 'yes') {
		wp_enqueue_script('jquery-mousewheel-js', plugins_url('assets/lightbox/lib/jquery.mousewheel-3.0.6.pack.js', __FILE__), array('jquery'));
		wp_enqueue_style('fancybox-main-css', plugins_url('assets/lightbox/source/jquery.fancybox.css', __FILE__), array());
		wp_enqueue_style('fancybox-buttons-css', plugins_url('assets/lightbox/source/helpers/jquery.fancybox-buttons.css', __FILE__), array());
		wp_enqueue_style('fancybox-thumbs-css', plugins_url('assets/lightbox/source/helpers/jquery.fancybox-thumbs.css', __FILE__), array());
		wp_enqueue_script('fancybox-main-js', plugins_url('assets/lightbox/source/jquery.fancybox.pack.js', __FILE__), array('jquery'));
		wp_enqueue_script('fancybox-buttons-js', plugins_url('assets/lightbox/source/helpers/jquery.fancybox-buttons.js', __FILE__), array('jquery'));
		wp_enqueue_script('fancybox-media-js', plugins_url('assets/lightbox/source/helpers/jquery.fancybox-media.js', __FILE__), array('jquery'));
		wp_enqueue_script('fancybox-thumbs-js', plugins_url('assets/lightbox/source/helpers/jquery.fancybox-thumbs.js', __FILE__), array('jquery'));
		wp_enqueue_script('customize-posts-lightnox-js', plugins_url('assets/customize-posts-lightnox.js', __FILE__), array('jquery'));
	}
}
add_action('wp_enqueue_scripts', 'my_scripts_method');

function customizeposts_shortcode_scripts() {
	//wp_register_script('customize-post', plugins_url("customize-post/assets/bootstrap/js/bootstrap.min.js"));
	wp_register_script('bootstrap', plugins_url('assets/bootstrap/js/bootstrap.js', __FILE__), array('jquery', 'jquery-ui-core'), '20120208', true);
	//wp_register_style('customize-post', plugins_url("customize-post/assets/bootstrap/css/bootstrap.css"));
	//wp_enqueue_style('customize-post');
}
add_action('wp_enqueue_scripts', 'customizeposts_shortcode_scripts');
/**
 * Register our users' custom post types.
 *
 * @since 0.5.0
 *
 * @internal
 */
function customizeposts_create_custom_post_types() {
	$cpts = get_option('customizeposts_post_types');
	if (empty($cpts)) {
		return;
	}
	/**
	 * Fires before the start of the post type registrations.
	 *
	 * @since 1.3.0
	 *
	 * @param array $cpts Array of post types to register.
	 */
	do_action('customizeposts_pre_register_post_types', $cpts);
	if (is_array($cpts)) {
		foreach ($cpts as $post_type) {
			customizeposts_register_single_post_type($post_type);
		}
	}
	/**
	 * Fires after the completion of the post type registrations.
	 *
	 * @since 1.3.0
	 *
	 * @param array $cpts Array of post types registered.
	 */
	do_action('customizeposts_post_register_post_types', $cpts);
}
add_action('init', 'customizeposts_create_custom_post_types', 10); // Leave on standard init for legacy purposes.
/**
 * Helper function to register the actual post_type.
 *
 * @since 1.0.0
 *
 * @internal
 *
 * @param array $post_type Post type array to register.
 * @return null Result of register_post_type.
 */
function customizeposts_register_single_post_type($post_type = array()) {
	/**
	 * Filters the map_meta_cap value.
	 *
	 * @since 1.0.0
	 *
	 * @param bool   $value     True.
	 * @param string $name      Post type name being registered.
	 * @param array  $post_type All parameters for post type registration.
	 */
	$post_type['map_meta_cap'] = apply_filters('customizeposts_map_meta_cap', true, $post_type['name'], $post_type);
	/**
	 * Filters custom supports parameters for 3rd party plugins.
	 *
	 * @since 1.0.0
	 *
	 * @param array  $value     Empty array to add supports keys to.
	 * @param string $name      Post type slug being registered.
	 * @param array  $post_type Array of post type arguments to be registered.
	 */
	$user_supports_params = apply_filters('customizeposts_user_supports_params', array(), $post_type['name'], $post_type);
	if (is_array($user_supports_params) && !empty($user_supports_params)) {
		if (is_array($post_type['supports'])) {
			$post_type['supports'] = array_merge($post_type['supports'], $user_supports_params);
		} else {
			$post_type['supports'] = array(
				$user_supports_params,
			);
		}
	}
	$yarpp = false; // Prevent notices.
	if (!empty($post_type['custom_supports'])) {
		$custom = explode(',', $post_type['custom_supports']);
		foreach ($custom as $part) {
			// We'll handle YARPP separately.
			if (in_array($part, array(
				'YARPP',
				'yarpp',
			))) {
				$yarpp = true;
				continue;
			}
			$post_type['supports'][] = trim($part);
		}
	}
	if (in_array('none', $post_type['supports'])) {
		$post_type['supports'] = false;
	}
	$labels = array(
		'name' => $post_type['label'],
		'singular_name' => $post_type['singular_label'],
	);
	$preserved = customizeposts_get_preserved_keys('post_types');
	foreach ($post_type['labels'] as $key => $label) {
		if (!empty($label)) {
			if ('parent' === $key) {
				$labels['parent_item_colon'] = $label;
			} else {
				$labels[$key] = $label;
			}
		} elseif (empty($label) && in_array($key, $preserved)) {
			$labels[$key] = customizeposts_get_preserved_label('post_types', $key, $post_type['label'], $post_type['singular_label']);
		}
	}
	$has_archive = get_disp_boolean($post_type['has_archive']);
	if (!empty($post_type['has_archive_string'])) {
		$has_archive = $post_type['has_archive_string'];
	}
	$show_in_menu = get_disp_boolean($post_type['show_in_menu']);
	if (!empty($post_type['show_in_menu_string'])) {
		$show_in_menu = $post_type['show_in_menu_string'];
	}
	$rewrite = get_disp_boolean($post_type['rewrite']);
	if (false !== $rewrite) {
		// Core converts to an empty array anyway, so safe to leave this instead of passing in boolean true.
		$rewrite = array();
		$rewrite['slug'] = (!empty($post_type['rewrite_slug'])) ? $post_type['rewrite_slug'] : $post_type['name'];
		$rewrite['with_front'] = ('false' === disp_boolean($post_type['rewrite_withfront'])) ? false : true;
	}
	$menu_icon = (!empty($post_type['menu_icon'])) ? $post_type['menu_icon'] : null;
	if (in_array($post_type['query_var'], array(
		'true',
		'false',
		'0',
		'1',
	))) {
		$post_type['query_var'] = get_disp_boolean($post_type['query_var']);
	}
	if (!empty($post_type['query_var_slug'])) {
		$post_type['query_var'] = $post_type['query_var_slug'];
	}
	$menu_position = null;
	if (!empty($post_type['menu_position'])) {
		$menu_position = (int) $post_type['menu_position'];
	}
	$public = get_disp_boolean($post_type['public']);
	if (!empty($post_type['exclude_from_search'])) {
		$exclude_from_search = get_disp_boolean($post_type['exclude_from_search']);
	} else {
		$exclude_from_search = (false === $public) ? true : false;
	}
	$queryable = (!empty($post_type['publicly_queryable']) && isset($post_type['publicly_queryable'])) ? get_disp_boolean($post_type['publicly_queryable']) : $public;
	if (empty($post_type['show_in_nav_menus'])) {
		// Defaults to value of public.
		$post_type['show_in_nav_menus'] = $public;
	}
	if (empty($post_type['show_in_rest'])) {
		$post_type['show_in_rest'] = false;
	}
	$rest_base = null;
	if (!empty($post_type['rest_base'])) {
		$rest_base = $post_type['rest_base'];
	}
	$args = array(
		'labels' => $labels,
		'description' => $post_type['description'],
		'public' => get_disp_boolean($post_type['public']),
		'publicly_queryable' => $queryable,
		'show_ui' => get_disp_boolean($post_type['show_ui']),
		'show_in_nav_menus' => get_disp_boolean($post_type['show_in_nav_menus']),
		'has_archive' => $has_archive,
		'show_in_menu' => $show_in_menu,
		'show_in_rest' => get_disp_boolean($post_type['show_in_rest']),
		'rest_base' => $rest_base,
		'exclude_from_search' => $exclude_from_search,
		'capability_type' => $post_type['capability_type'],
		'map_meta_cap' => $post_type['map_meta_cap'],
		'hierarchical' => get_disp_boolean($post_type['hierarchical']),
		'rewrite' => $rewrite,
		'menu_position' => $menu_position,
		'menu_icon' => $menu_icon,
		'query_var' => $post_type['query_var'],
		'supports' => $post_type['supports'],
		'taxonomies' => $post_type['taxonomies'],
	);
	if (true === $yarpp) {
		$args['yarpp_support'] = $yarpp;
	}
	/**
	 * Filters the arguments used for a post type right before registering.
	 *
	 * @since 1.0.0
	 * @since 1.3.0 Added original passed in values array
	 *
	 * @param array  $args      Array of arguments to use for registering post type.
	 * @param string $value     Post type slug to be registered.
	 * @param array  $post_type Original passed in values for post type.
	 */
	$args = apply_filters('customizeposts_pre_register_post_type', $args, $post_type['name'], $post_type);
	return register_post_type($post_type['name'], $args);
}
/**
 * Register our users' custom taxonomies.
 *
 * @since 0.5.0
 *
 * @internal
 */
function customizeposts_create_custom_taxonomies() {
	$taxes = get_option('customizeposts_taxonomies');
	if (empty($taxes)) {
		return;
	}
	/**
	 * Fires before the start of the taxonomy registrations.
	 *
	 * @since 1.3.0
	 *
	 * @param array $taxes Array of taxonomies to register.
	 */
	do_action('customizeposts_pre_register_taxonomies', $taxes);
	if (is_array($taxes)) {
		foreach ($taxes as $tax) {
			customizeposts_register_single_taxonomy($tax);
		}
	}
	/**
	 * Fires after the completion of the taxonomy registrations.
	 *
	 * @since 1.3.0
	 *
	 * @param array $taxes Array of taxonomies registered.
	 */
	do_action('customizeposts_post_register_taxonomies', $taxes);
}
add_action('init', 'customizeposts_create_custom_taxonomies', 9); // Leave on standard init for legacy purposes.
/**
 * Helper function to register the actual taxonomy.
 *
 * @since 1.0.0
 *
 * @internal
 *
 * @param array $taxonomy Taxonomy array to register.
 * @return null Result of register_taxonomy.
 */
function customizeposts_register_single_taxonomy($taxonomy = array()) {
	$labels = array(
		'name' => $taxonomy['label'],
		'singular_name' => $taxonomy['singular_label'],
	);
	$description = '';
	if (!empty($taxonomy['description'])) {
		$description = $taxonomy['description'];
	}
	$preserved = customizeposts_get_preserved_keys('taxonomies');
	foreach ($taxonomy['labels'] as $key => $label) {
		if (!empty($label)) {
			$labels[$key] = $label;
		} elseif (empty($label) && in_array($key, $preserved)) {
			$labels[$key] = customizeposts_get_preserved_label('taxonomies', $key, $taxonomy['label'], $taxonomy['singular_label']);
		}
	}
	$rewrite = get_disp_boolean($taxonomy['rewrite']);
	if (false !== get_disp_boolean($taxonomy['rewrite'])) {
		$rewrite = array();
		$rewrite['slug'] = (!empty($taxonomy['rewrite_slug'])) ? $taxonomy['rewrite_slug'] : $taxonomy['name'];
		$rewrite['with_front'] = ('false' === disp_boolean($taxonomy['rewrite_withfront'])) ? false : true;
		$rewrite['hierarchical'] = ('true' === disp_boolean($taxonomy['rewrite_hierarchical'])) ? true : false;
	}
	if (in_array($taxonomy['query_var'], array(
		'true',
		'false',
		'0',
		'1',
	))) {
		$taxonomy['query_var'] = get_disp_boolean($taxonomy['query_var']);
	}
	if (true === $taxonomy['query_var'] && !empty($taxonomy['query_var_slug'])) {
		$taxonomy['query_var'] = $taxonomy['query_var_slug'];
	}
	$public = (!empty($taxonomy['public']) && false === get_disp_boolean($taxonomy['public'])) ? false : true;
	$show_admin_column = (!empty($taxonomy['show_admin_column']) && false !== get_disp_boolean($taxonomy['show_admin_column'])) ? true : false;
	$show_in_menu = (!empty($taxonomy['show_in_menu']) && false !== get_disp_boolean($taxonomy['show_in_menu'])) ? true : false;
	if (empty($taxonomy['show_in_menu'])) {
		$show_in_menu = get_disp_boolean($taxonomy['show_ui']);
	}
	$show_in_nav_menus = (!empty($taxonomy['show_in_nav_menus']) && false !== get_disp_boolean($taxonomy['show_in_nav_menus'])) ? true : false;
	if (empty($taxonomy['show_in_nav_menus'])) {
		$show_in_nav_menus = $public;
	}
	$show_in_rest = (!empty($taxonomy['show_in_rest']) && false !== get_disp_boolean($taxonomy['show_in_rest'])) ? true : false;
	$show_in_quick_edit = (!empty($taxonomy['show_in_quick_edit']) && false !== get_disp_boolean($taxonomy['show_in_quick_edit'])) ? true : false;
	$rest_base = null;
	if (!empty($taxonomy['rest_base'])) {
		$rest_base = $taxonomy['rest_base'];
	}
	$args = array(
		'labels' => $labels,
		'label' => $taxonomy['label'],
		'description' => $description,
		'public' => $public,
		'hierarchical' => get_disp_boolean($taxonomy['hierarchical']),
		'show_ui' => get_disp_boolean($taxonomy['show_ui']),
		'show_in_menu' => $show_in_menu,
		'show_in_nav_menus' => $show_in_nav_menus,
		'query_var' => $taxonomy['query_var'],
		'rewrite' => $rewrite,
		'show_admin_column' => $show_admin_column,
		'show_in_rest' => $show_in_rest,
		'rest_base' => $rest_base,
		'show_in_quick_edit' => $show_in_quick_edit,
	);
	$object_type = (!empty($taxonomy['object_types'])) ? $taxonomy['object_types'] : '';
	/**
	 * Filters the arguments used for a taxonomy right before registering.
	 *
	 * @since 1.0.0
	 * @since 1.3.0 Added original passed in values array
	 *
	 * @param array  $args     Array of arguments to use for registering taxonomy.
	 * @param string $value    Taxonomy slug to be registered.
	 * @param array  $taxonomy Original passed in values for taxonomy.
	 */
	$args = apply_filters('customizeposts_pre_register_taxonomy', $args, $taxonomy['name'], $taxonomy);
	return register_taxonomy($taxonomy['name'], $object_type, $args);
}
/**
 * Construct and output tab navigation.
 *
 * @since 1.0.0
 *
 * @param string $page Whether it's the CPT or Taxonomy page.
 */
function customizeposts_settings_tab_menu($page = 'post_types') {
	/**
	 * Filters the tabs to render on a given page.
	 *
	 * @since 1.3.0
	 *
	 * @param array  $value Array of tabs to render.
	 * @param string $page  Current page being displayed.
	 */
	$tabs = (array) apply_filters('customizeposts_get_tabs', array(), $page);
	if (!empty($tabs['page_title'])) {
		printf('<h1>%s</h1><h2 class="nav-tab-wrapper">', $tabs['page_title']);
	}
	foreach ($tabs['tabs'] as $tab) {
		printf('<a class="%s" href="%s" aria-selected="%s">%s</a>', implode(' ', $tab['classes']), $tab['url'], $tab['aria-selected'], $tab['text']);
	}
	echo '</h2>';
}
/**
 * Convert our old settings to the new options keys.
 *
 * These are left with standard get_option/update_option function calls for legacy and pending update purposes.
 *
 * @since 1.0.0
 *
 * @internal
 *
 * @return bool Whether or not options were successfully updated.
 */
function customizeposts_convert_settings() {
	if (defined('DOING_AJAX') && DOING_AJAX) {
		return;
	}
	$retval = '';
	if (false === get_option('customizeposts_post_types') && ($post_types = get_option('cpt_custom_post_types'))) {
		$new_post_types = array();
		foreach ($post_types as $type) {
			$new_post_types[$type['name']] = $type; // This one assigns the # indexes. Named arrays are our friend.
			$new_post_types[$type['name']]['supports'] = (!empty($type[0])) ? $type[0] : array(); // Especially for multidimensional arrays.
			$new_post_types[$type['name']]['taxonomies'] = (!empty($type[1])) ? $type[1] : array();
			$new_post_types[$type['name']]['labels'] = (!empty($type[2])) ? $type[2] : array();
			unset($new_post_types[$type['name']][0], $new_post_types[$type['name']][1], $new_post_types[$type['name']][2]); // Remove our previous indexed versions.
		}
		$retval = update_option('customizeposts_post_types', $new_post_types);
	}
	if (false === get_option('customizeposts_taxonomies') && ($taxonomies = get_option('cpt_custom_tax_types'))) {
		$new_taxonomies = array();
		foreach ($taxonomies as $tax) {
			$new_taxonomies[$tax['name']] = $tax; // Yep, still our friend.
			$new_taxonomies[$tax['name']]['labels'] = $tax[0]; // Taxonomies are the only thing with
			$new_taxonomies[$tax['name']]['object_types'] = $tax[1]; // "tax" in the name that I like.
			unset($new_taxonomies[$tax['name']][0], $new_taxonomies[$tax['name']][1]);
		}
		$retval = update_option('customizeposts_taxonomies', $new_taxonomies);
	}
	if (!empty($retval)) {
		flush_rewrite_rules();
	}
	return $retval;
}
add_action('admin_init', 'customizeposts_convert_settings');
/**
 * Return a notice based on conditions.
 *
 * @since 1.0.0
 *
 * @param string $action       The type of action that occurred.
 * @param string $object_type  Whether it's from a post type or taxonomy.
 * @param bool   $success      Whether the action succeeded or not.
 * @param string $custom       Custom message if necessary.
 * @return bool|string false on no message, else HTML div with our notice message.
 */
function customizeposts_admin_notices($action = '', $object_type = '', $success = true, $custom = '') {
	$class = array();
	$class[] = ($success) ? 'updated' : 'error';
	$class[] = 'notice is-dismissible';
	$object_type = esc_attr($object_type);
	$messagewrapstart = '<div id="message" class="' . implode(' ', $class) . '"><p>';
	$message = '';
	$messagewrapend = '</p></div>';
	if ('add' == $action) {
		if ($success) {
			$message .= sprintf(__('%s has been successfully added', 'customizeposts'), $object_type);
		} else {
			$message .= sprintf(__('%s has failed to be added', 'customizeposts'), $object_type);
		}
	} elseif ('update' == $action) {
		if ($success) {
			$message .= sprintf(__('%s has been successfully updated', 'customizeposts'), $object_type);
		} else {
			$message .= sprintf(__('%s has failed to be updated', 'customizeposts'), $object_type);
		}
	} elseif ('delete' == $action) {
		if ($success) {
			$message .= sprintf(__('%s has been successfully deleted', 'customizeposts'), $object_type);
		} else {
			$message .= sprintf(__('%s has failed to be deleted', 'customizeposts'), $object_type);
		}
	} elseif ('import' == $action) {
		if ($success) {
			$message .= sprintf(__('%s has been successfully imported', 'customizeposts'), $object_type);
		} else {
			$message .= sprintf(__('%s has failed to be imported', 'customizeposts'), $object_type);
		}
	} elseif ('error' == $action) {
		if (!empty($custom)) {
			$message = $custom;
		}
	}
	if ($message) {
		/**
		 * Filters the custom admin notice for customizeposts.
		 *
		 * @since 1.0.0
		 *
		 * @param string $value            Complete HTML output for notice.
		 * @param string $action           Action whose message is being generated.
		 * @param string $message          The message to be displayed.
		 * @param string $messagewrapstart Beginning wrap HTML.
		 * @param string $messagewrapend   Ending wrap HTML.
		 */
		return apply_filters('customizeposts_admin_notice', $messagewrapstart . $message . $messagewrapend, $action, $message, $messagewrapstart, $messagewrapend);
	}
	return false;
}
/**
 * Return array of keys needing preserved.
 *
 * @since 1.0.5
 *
 * @param string $type Type to return. Either 'post_types' or 'taxonomies'.
 * @return array Array of keys needing preservered for the requested type.
 */
function customizeposts_get_preserved_keys($type = '') {
	$preserved_labels = array(
		'post_types' => array(
			'add_new_item',
			'edit_item',
			'new_item',
			'view_item',
			'all_items',
			'search_items',
			'not_found',
			'not_found_in_trash',
		),
		'taxonomies' => array(
			'search_items',
			'popular_items',
			'all_items',
			'parent_item',
			'parent_item_colon',
			'edit_item',
			'update_item',
			'add_new_item',
			'new_item_name',
			'separate_items_with_commas',
			'add_or_remove_items',
			'choose_from_most_used',
		),
	);
	return (!empty($type)) ? $preserved_labels[$type] : array();
}
/**
 * Return label for the requested type and label key.
 *
 * @since 1.0.5
 *
 * @param string $type Type to return. Either 'post_types' or 'taxonomies'.
 * @param string $key Requested label key.
 * @param string $plural Plural verbiage for the requested label and type.
 * @param string $singular Singular verbiage for the requested label and type.
 * @return string Internationalized default label.
 */
function customizeposts_get_preserved_label($type = '', $key = '', $plural = '', $singular = '') {
	$preserved_labels = array(
		'post_types' => array(
			'add_new_item' => sprintf(__('Add new %s', 'customizeposts'), $singular),
			'edit_item' => sprintf(__('Edit %s', 'customizeposts'), $singular),
			'new_item' => sprintf(__('New %s', 'customizeposts'), $singular),
			'view_item' => sprintf(__('View %s', 'customizeposts'), $singular),
			'all_items' => sprintf(__('All %s', 'customizeposts'), $plural),
			'search_items' => sprintf(__('Search %s', 'customizeposts'), $plural),
			'not_found' => sprintf(__('No %s found.', 'customizeposts'), $plural),
			'not_found_in_trash' => sprintf(__('No %s found in trash.', 'customizeposts'), $plural),
		),
		'taxonomies' => array(
			'search_items' => sprintf(__('Search %s', 'customizeposts'), $plural),
			'popular_items' => sprintf(__('Popular %s', 'customizeposts'), $plural),
			'all_items' => sprintf(__('All %s', 'customizeposts'), $plural),
			'parent_item' => sprintf(__('Parent %s', 'customizeposts'), $singular),
			'parent_item_colon' => sprintf(__('Parent %s:', 'customizeposts'), $singular),
			'edit_item' => sprintf(__('Edit %s', 'customizeposts'), $singular),
			'update_item' => sprintf(__('Update %s', 'customizeposts'), $singular),
			'add_new_item' => sprintf(__('Add new %s', 'customizeposts'), $singular),
			'new_item_name' => sprintf(__('New %s name', 'customizeposts'), $singular),
			'separate_items_with_commas' => sprintf(__('Separate %s with commas', 'customizeposts'), $plural),
			'add_or_remove_items' => sprintf(__('Add or remove %s', 'customizeposts'), $plural),
			'choose_from_most_used' => sprintf(__('Choose from the most used %s', 'customizeposts'), $plural),
		),
	);
	return $preserved_labels[$type][$key];
}

add_action('admin_footer', 'insert_footer_wpse_51023');
function insert_footer_wpse_51023() {
	echo '<div style="width:88%;margin-left:12%;"><img src="https://www.eskimokids.com/images/discountbanner-998x330.php" height="100" style="width:100%;margin-bottom:2%;"/></div>';
}
function dd($data) {
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}
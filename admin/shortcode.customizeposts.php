<?php
/**
 * Customize Post Shortcode.
 *
 * @package customizeposts
 * @subpackage Admin
 * @author Citytechcorp.com
 * @since 1.0.0
 */

/**
 * Customize Post Shortcode
 *
 * @since 1.0.0
 */
if (!defined('ABSPATH')) {
	exit;
}
/**
 * Main class involved shortcode generator.
 **/
class Customize_Post_Shortcode {

	function customizeposts_shortcode_extractor($atts) {
		extract(shortcode_atts(array('limit' => -1, 'category' => ''), $atts));
		return $atts;
	}
	function customizeposts_comma_to_array($coma) {
		$rm = explode(',', $coma);
		return $rm;
	}
	function customizeposts_tax_query($taxes, $terms) {
		return array('tax_query' => array(
			array(
				'taxonomy' => $taxes,
				'field' => 'slug',
				'terms' => $this->customizeposts_comma_to_array($terms),
				'include_children' => true,
				'operator' => 'IN',
			),
		),
		);
	}
	function customizeposts_order_by($order, $orderby) {
		return array(
			'orderby' => $orderby,
			'order' => $order,
		);
	}
	function customizeposts_selected_post_by_id($id) {
		return array(
			'post__in' => $this->customizeposts_comma_to_array($id),
		);
	}
	function customizeposts_pagination($param) {
		return array('posts_per_page' => $param,
			'paged' => get_query_var('paged') ? get_query_var('paged') : 1);
	}
	function customizeposts_lightbox_image($param, $make_params) {
		$loop = new WP_Query($make_params);
		if ($loop->have_posts()) {
			while ($loop->have_posts()) {
				$loop->the_post();
				$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
				?>
				<div class="<?php echo $param['boot_param_list']; ?>">
				<a class="fancybox"
				   rel="fancybox-thumb"
				   href="<?php echo $featured_image; ?>"
				   title="<?php the_title()?>">
					<img src="<?php echo $featured_image; ?>" alt="" height="<?php echo $param['thumb_height']; ?>" width="<?php echo $param['thumb_width']; ?>"/>
				</a>
				<div class="cp-image-title"><?php the_title()?></div>
				</div>
				<?php }
			echo '</div>';
		}
		wp_reset_query();
	}
	function customizeposts_lightbox_video($param, $make_params) {
		$loop = new WP_Query($make_params);
		if ($loop->have_posts()) {
			while ($loop->have_posts()) {
				$loop->the_post();
				$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
				$getPost = get_the_content();
				$postwithbreaks = wpautop($getPost, true);
				$postwithbreaks = strip_tags($postwithbreaks);
				?>
				<div class="<?php echo $param['boot_param_list']; ?>">
				<a class="video-light-box fancybox.iframe" href="<?php echo $postwithbreaks; ?>">
					<img src="http://img.youtube.com/vi/L9szn1QQfas/0.jpg" height="<?php echo $param['thumb_height']; ?>" width="<?php echo $param['thumb_width']; ?>"/>
				</a>
				<div class="cp-video-title"><?php the_title()?></div>
				</div>
					<?php }
			echo '</div>';
		}
		wp_reset_query();
	}
	function customizeposts_query_loop_blocks($param) {
		echo '<div class="row">'; //Start Row
		$make_params = array(
			'post_type' => $this->customizeposts_comma_to_array($param['post_type']),
			'post_status' => 'publish',
		);
		if (isset($param['order'])) {
			$make_params = array_merge($make_params, $this->customizeposts_order_by($param['order'], $param['orderby']));
		}
		if (isset($param['posts_per_page'])) {
			$make_params = array_merge($make_params, $this->customizeposts_pagination($param['posts_per_page']));
		}
		if (isset($param['post__in']) || $param['post__in'] != '') {
			$make_params = array_merge($make_params, $this->customizeposts_selected_post_by_id($param['post__in']));
		}
		if (isset($param['taxonomy_name'])) {
			$make_params = array_merge($make_params, $this->customizeposts_tax_query($param['taxonomy_name'], $param['terms_name']));
		}
		$loop = new WP_Query($make_params);
		if ($loop->have_posts()) {
			while ($loop->have_posts()) {
				$loop->the_post();
				?>
				<div class="<?php echo $param['boot_param_list']; ?>">
					<?php the_title('<h1>', '</h1>');?>
					<?php if ($param['featured_image'] == 'true' || isset($param['featured_image_size'])) {
					if (has_post_thumbnail()) {?>
						<div class="featured-image-post" >
							<?php the_post_thumbnail(array($param['featured_image_size']), array('class' => 'img-responsive'));?>
						</div>
						<?php }
				}
				if (!isset($param['content_length'])) {
					the_content();
				} else {
					$getPost = get_the_content();
					$postwithbreaks = wpautop($getPost, true);
					echo substr(strip_tags($postwithbreaks), 0, $param['content_length']) . '[...]';
				}
				?>
					<p><a role="button" class="btn btn-default" href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">View details »</a></p>
				</div>
				<?php }
			echo '</div>'; // End of ROW.?>
				<nav aria-label="Page navigation">
					<?php $big = 999999999;
			echo paginate_links(array(
				'base' => str_replace($big, '%#%', get_pagenum_link($big)),
				'format' => '?paged=%#%',
				'current' => max(1, get_query_var('paged')),
				'total' => $loop->max_num_pages,
			));
			?>
					</nav>

					<?php }

		wp_reset_query();

	}
	function customizeposts_query_loop_lightbox($param) {
		echo '<div class="row lightbox-wrapper">';
		$make_params = array(
			'post_type' => $this->customizeposts_comma_to_array($param['post_type']),
			'post_status' => 'publish',
		);
		if (isset($param['order'])) {
			$make_params = array_merge($make_params, $this->customizeposts_order_by($param['order'], $param['orderby']));
		}
		if (isset($param['posts_per_page'])) {
			$make_params = array_merge($make_params, $this->customizeposts_pagination($param['posts_per_page']));
		}
		if (isset($param['post__in']) || $param['post__in'] != '') {
			$make_params = array_merge($make_params, $this->customizeposts_selected_post_by_id($param['post__in']));
		}
		if (isset($param['taxonomy_name'])) {
			$make_params = array_merge($make_params, $this->customizeposts_tax_query($param['taxonomy_name'], $param['terms_name']));
		}
		switch ($param['display_type']) {
		case 'video':
			$this->customizeposts_lightbox_video($param, $make_params);
			break;
		case 'image':
			$this->customizeposts_lightbox_image($param, $make_params);
			break;
		default:
			echo '<h3>Please specify <em>display_type</em> properly.</h3>';
			break;
		}

	}
	function customizeposts_query_loop_slider($param) {
		$nimbus_slideshow_interval = 3;
		$nimbus_slideshow_pause = 'hover';
		$nimbus_slideshow_wrap = true;
		$make_params = array(
			'post_type' => $this->customizeposts_comma_to_array($param['post_type']),
			'post_status' => 'publish',
		);
		if (isset($param['order'])) {
			$make_params = array_merge($make_params, $this->customizeposts_order_by($param['order'], $param['orderby']));
		}
		if (isset($param['posts_per_page'])) {
			$make_params = array_merge($make_params, $this->customizeposts_pagination($param['posts_per_page']));
		}
		if (isset($param['post__in']) || $param['post__in'] != '') {
			$make_params = array_merge($make_params, $this->customizeposts_selected_post_by_id($param['post__in']));
		}
		if (isset($param['taxonomy_name'])) {
			$make_params = array_merge($make_params, $this->customizeposts_tax_query($param['taxonomy_name'], $param['terms_name']));
		}
		$custom_query = new WP_Query($make_params);

		?>
		<div id="slideshow" class="carousel slide" data-ride="carousel" data-interval="<?php echo $nimbus_slideshow_interval * 1000 ?>" data-pause="<?php echo $nimbus_slideshow_pause ?>" data-wrap=<?php echo $nimbus_slideshow_wrap ?>>
		<div class="carousel-inner cp-<?php echo $param['post_type']; ?>">
								<?php $i = 0;
		while ($custom_query->have_posts()) {
			$custom_query->the_post();
			$post_id = get_the_ID();
			$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));

			?>
			<div class="item <?php if ($i == 0) {?>active <?php }?>" id="slide-<?php echo $post_id ?>" style="background-image:url(<?php echo $featured_image ?>);">
				<div class="carousel-caption-text">
					<div class="container">
						<div class="slide_h1_wrap"><h1 class="headline headline-<?php echo $post_id ?>"><?php the_title();?></h1></div>
						<?php $thecontent = get_the_content();?>
						<?php if (!empty($thecontent)) {?>
						<div class="descriptions descriptions-<?php echo $post_id ?>"><?php the_content();?></div>
						<?php }?>
					</div>
				</div>
			</div>
		<?php $i++;}?>
			</div>
			<!-- Indicators -->
			<a class="left carousel-control" href="#slideshow" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slideshow" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
			<ol class="carousel-indicators">
				<?php for ($j = 0; $j < $i; $j++) {?>
				<li data-target="#slideshow" data-slide-to="<?php echo $j ?>" <?php if ($j == 0) {?>class="active"<?php }?>></li>
				<?php }?>
			</ol>
		</div><!-- /.carousel -->
		<?php wp_reset_postdata();
	}

	function customizeposts_default_message() {
		echo '<h2>Please provide a valid <em>code_type</em> to understand your demand.</h2>
							<a href="' . home_url() . '/wp-admin/admin.php?page=shortcode-listing" target="_blank">Click here</a> for help to build a correct shortcode.';
	}

	function customizeposts_display($atts) {
		$params = $this->customizeposts_shortcode_extractor($atts);
		//dd($params);
		switch ($params['code_type']) {
		case 'block_listing':
			$this->customizeposts_query_loop_blocks($params);
			break;
		case 'block_lightbox':
			$this->customizeposts_query_loop_lightbox($params);
			break;
		case 'block_slider':
			$this->customizeposts_query_loop_slider($params);
			break;
		default:
			$this->customizeposts_default_message();
			break;
		}
	}
}

/**
 ** Generator Shortcode
 ** Customize post
 */
function customizeposts_shortcode_generator($atts, $content) {
	$obj = new Customize_Post_Shortcode();
	$obj->customizeposts_display($atts);
}
add_shortcode('customizeposts', 'customizeposts_shortcode_generator');

?>


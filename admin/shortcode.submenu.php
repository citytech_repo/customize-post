<?php
/**
 * Customize Post Shortcode Menu Page.
 *
 * @package customizeposts
 * @subpackage Admin
 * @author Citytechcorp.com
 * @since 1.0.0
 */

/**
 * Customize Post Shortcode Menu Page
 *
 * @since 1.0.0
 */
if (!defined('ABSPATH')) {
	exit;
}

function shortcode_listing() {
	add_option('bs_mode', '', '', 'yes');
	add_option('fb_mode', '', '', 'yes');
	//add_option( 'purchase_mail', '', '', 'yes' );
	//add_option( 'insta_access_token', '', '', 'yes' );
	$msg = '';

	if (isset($_POST['submit'])) {
		foreach ($_POST as $key => $value) {
			if ($key != 'submit') {
				update_option($key, $value);
			}

		}
		$msg = '<div id="message" class="update notice notice-success is-dismissible"><p><strong style="color:green;">Success!</strong> You have loaded Bootstrap as well.</p></div>';
	}
	$bs_mode = get_option('bs_mode');
	$fb_mode = get_option('fb_mode');
	//$purchase_mail=get_option( 'purchase_mail' );
	//$insta_access_token=get_option( 'insta_access_token' );

	?>
			<script type="text/javascript">
		/*jQuery(function($){
			var bootstrap3_enabled = (typeof jQuery().emulateTransitionEnd == 'function');
			if(bootstrap3_enabled==false){
				$('.customizeposts-listings').prepend('<div id="message" class="error notice notice-success is-dismissible"><p><strong style="color:red;">Attention Please!</strong> You have to load bootstrap for better experience at front end.</p></div>');
			}
		});*/
	</script>
	<div id="wpbody" role="main">
		<div aria-label="Main content" id="wpbody-content">
			<div class="wrap customizeposts-listings">
				<h1>Frontend Short code</h1>
				<?php echo $msg; ?>
				<div id="poststuff">
					<div class="customizeposts-section postbox">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="screen-reader-text">Toggle panel: Basic settings</span>
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h2 class="hndle">
							<span>Basic settings</span>
						</h2>
						<div class="inside">
							<?php echo $bootstrap;if ($bootstrap != 'true') { ?>
							<div class="main bootstrap-loader">
								<h3>Load boot strap</h3>
								<form action="" method="post">
									<label><b>Load Bootstrap</b></label></br>
									<select name="bs_mode" style="width:500px;">
										<?php if ($bs_mode == 'yes') {?>
										<option value="yes" selected="selected">Yes</option>
										<option value="no" >No</option>
										<?php } elseif ($bs_mode == 'no') {?>
										<option value="yes">Yes</option>
										<option value="no" selected="selected">No</option>
										<?php } else {?>
										<option value="">Not Yet Loaded</option>
										<option value="yes" >Yes</option>
										<option value="no" >No</option>
										<?php }?>
									</select>
									<div style="clear:both;"></div>
									<label><b>Load Fancybox Libraries</b></label></br>
									<select name="fb_mode" style="width:500px;">
										<?php if ($fb_mode == 'yes') {?>
										<option value="yes" selected="selected">Yes</option>
										<option value="no" >No</option>
										<?php } elseif ($fb_mode == 'no') {?>
										<option value="yes">Yes</option>
										<option value="no" selected="selected">No</option>
										<?php } else {?>
										<option value="">Not Yet Loaded</option>
										<option value="yes" >Yes</option>
										<option value="no" >No</option>
										<?php }?>
									</select>
									<div style="clear:both;"></div>
									<p></p>
									<input name="submit" type="submit" value="Save Credentials" class="button button-primary" />
								</form>

							</div>
							<?php }?>
						</div>
					</div>
					<div style="clear: both;"></div>
					<div class="customizeposts-section postbox">
						<button type="button" class="handlediv button-link" aria-expanded="true">
							<span class="screen-reader-text">Toggle panel: Basic settings</span>
							<span class="toggle-indicator" aria-hidden="true"></span>
						</button>
						<h2 class="hndle">
						<span>List Of shortcode</span>
						</h2>
						<div class="inside">
							<h3>Block Listing</h3>
							<code>[customizeposts code_type="block_listing"
							  post_type="product,post"
							  post__in="61,63,60"
							  taxonomy_name="product_category"
							  terms_name="appral,foot-ware,garments,lingerie"
							  posts_per_page="3"
							  boot_param_list="col-xs-6 col-lg-4 col-md-4"
							  featured_image="true"
							  featured_image_size="111,111"
							  content_length="200"
							  order="ASC"
							  orderby="rand"]</code>
							<table class="table table-hover" style="width: 100%;">
								<thead>
									<tr >
										<th>Params</th>
										<th>Functionality</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 50%;">code_type</td>
										<td style="width: 50%;">block_listing-> General Block Display.</td>
									</tr>
									<tr>
										<td style="width: 50%;">featured_image</td>
										<td style="width: 50%;">thumbnail,medium,large,medium_large,full, -> Featured Image size.
										Custom Size WXH = 175,175</td>
									</tr>
								</tbody>
							</table>
							<h3>Fancybox Listing</h3>
							<code>[customizeposts code_type="block_lightbox" display_type="video" thumb_height="250" thumb_width="300" post_type="lightbox" order="ASC" orderby="rand" boot_param_list="col-xs-6 col-lg-3 col-md-3"]</code>
							<table class="table table-hover" style="width: 100%;">
								<thead>
									<tr >
										<th>Params</th>
										<th>Functionality</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 50%;">code_type</td>
										<td style="width: 50%;">block_listing-> General Block Display.</td>
									</tr>
									<tr>
										<td style="width: 50%;">featured_image</td>
										<td style="width: 50%;">thumbnail,medium,large,medium_large,full, -> Featured Image size.
										Custom Size WXH = 175,175</td>
									</tr>
								</tbody>
							</table>
							<h3>Slider Shortcode</h3>
							<code>[customizeposts code_type="block_slider" post_type="slider" order="ASC" orderby="rand"]</code>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php }

?>
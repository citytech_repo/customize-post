<?php
/**
 * Customize Post Registered Content.
 *
 * @package customizeposts
 * @subpackage Listings
 * @author Citytechcorp.com
 * @since 1.1.0
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Output the content for the "Registered Types/Taxes" page.
 *
 * @since 1.1.0
 *
 * @internal
 */
function customizeposts_listings() {
	?>
		<div class="wrap customizeposts-listings">
			<?php
/**
	 * Fires right inside the wrap div for the listings screen.
	 *
	 * @since 1.3.0
	 */
	do_action('customizeposts_inside_listings_wrap');
	?>

			<h1><?php esc_html_e('Post Types and Taxonomies registered by Customize Post.', 'customizeposts');?></h1>
			<?php
$post_types = customizeposts_get_post_type_data();
	echo '<h2 id="post-types">' . esc_html__('Post Types', 'customizeposts') . '</h2>';
	if (!empty($post_types)) {
		?>
			<p><?php printf(esc_html__('customizeposts registered post types count total: %d', 'customizeposts'), count($post_types));?></p>

			<?php

		$post_type_table_heads = array(
			__('Post Type', 'customizeposts'),
			__('Settings', 'customizeposts'),
			__('Supports', 'customizeposts'),
			__('Taxonomies', 'customizeposts'),
			__('Labels', 'customizeposts'),
			__('Template Hierarchy', 'customizeposts'),
		);

		/**
		 * Fires before the listing of registered post type data.
		 *
		 * @since 1.1.0
		 */
		do_action('customizeposts_before_post_type_listing');
		?>
			<table class="wp-list-table widefat post-type-listing">
				<tr>
					<?php
foreach ($post_type_table_heads as $head) {
			echo '<th>' . esc_html($head) . '</th>';
		}?>
				</tr>
				<?php
$counter = 1;
		foreach ($post_types as $post_type => $post_type_settings) {

			$rowclass = (0 === $counter % 2) ? '' : 'alternate';

			$strings = array();
			$supports = array();
			$taxonomies = array();
			$archive = '';
			foreach ($post_type_settings as $settings_key => $settings_value) {
				if ('labels' === $settings_key) {
					continue;
				}

				if (is_string($settings_value)) {
					$strings[$settings_key] = $settings_value;
				} else {
					if ('supports' === $settings_key) {
						$supports[$settings_key] = $settings_value;
					}

					if ('taxonomies' === $settings_key) {
						$taxonomies[$settings_key] = $settings_value;

						// In case they are not associated from the post type settings.
						if (empty($taxonomies['taxonomies'])) {
							$taxonomies['taxonomies'] = get_object_taxonomies($post_type);
						}
					}
				}
				$archive = get_post_type_archive_link($post_type);
			}
			?>
						<tr class="<?php echo esc_attr($rowclass); ?>">
							<?php
$edit_path = 'admin.php?page=customizeposts_manage_post_types&action=edit&customizeposts_post_type=' . $post_type;
			$post_type_link_url = (is_network_admin()) ? network_admin_url($edit_path) : admin_url($edit_path);?>
							<td>
								<?php
printf(
				'<a href="%s">%s</a> | <a href="%s">%s</a><br/>',
				esc_attr($post_type_link_url),
				sprintf(
					esc_html__('Edit %s', 'customizeposts'),
					esc_html($post_type)
				),
				esc_attr(admin_url('admin.php?page=customizeposts_orderreorder&action=get_code#' . $post_type)),
				esc_html__('Get code', 'customizeposts')
			);

			if ($archive) {
				?>
								<a href="<?php echo esc_attr(get_post_type_archive_link($post_type)); ?>"><?php esc_html_e('View frontend archive', 'customizeposts');?></a>
								<?php }?>
							</td>
							<td>
								<?php
foreach ($strings as $key => $value) {
				printf('<strong>%s:</strong> ', esc_html($key));
				if (in_array($value, array('1', '0'))) {
					echo esc_html(disp_boolean($value));
				} else {
					echo esc_html($value);
				}
				echo '<br/>';
			}?>
							</td>
							<td>
								<?php
foreach ($supports['supports'] as $support) {
				echo esc_html($support) . '<br/>';
			}?>
							</td>
							<td>
								<?php
foreach ($taxonomies['taxonomies'] as $taxonomy) {
				echo esc_html($taxonomy) . '<br/>';
			}?>
							</td>
							<td>
								<?php
$maybe_empty = array_filter($post_type_settings['labels']);
			if (!empty($maybe_empty)) {
				foreach ($post_type_settings['labels'] as $key => $value) {
					if ('parent' === $key && array_key_exists('parent_item_colon', $post_type_settings['labels'])) {
						continue;
					}
					printf(
						'%s: %s<br/>',
						esc_html($key),
						esc_html($value)
					);
				}
			} else {
				esc_html_e('No custom labels to display', 'customizeposts');
			}
			?>
							</td>
							<td>
								<p><strong><?php esc_html_e('Archives file name examples.', 'customizeposts');?></strong><br/>
								archive-<?php echo esc_html($post_type); ?>.php<br/>
								archive.php<br/>
								index.php
								</p>

								<p><strong><?php esc_html_e('Single Posts file name examples.', 'customizeposts');?></strong><br/>
								single-<?php echo esc_html($post_type); ?>-post_slug.php (WP 4.4+) *<br/>
								single-<?php echo esc_html($post_type); ?>.php<br/>
								single.php<br/>
								singular.php (WP 4.3+)<br/>
								index.php
								</p>

								<p>
									<?php esc_html_e('*Replace "post_slug" with the slug of the actual post slug.', 'customizeposts');?>
								</p>

								<p><?php
printf(
				'<a href="https://developer.wordpress.org/themes/basics/template-hierarchy/">%s</a>',
				esc_html__('Template hierarchy Theme Handbook', 'customizeposts')
			);?>
								</p>
							</td>
						</tr>

					<?php
$counter++;
		}
		?>
				<tr>
					<?php
foreach ($post_type_table_heads as $head) {
			echo '<th>' . esc_html($head) . '</th>';
		}?>
				</tr>
			</table>
			<?php
/**
		 * Fires after the listing of registered post type data.
		 *
		 * @since 1.3.0
		 */
		do_action('customizeposts_after_post_type_listing');
	} else {

		/**
		 * Fires when there are no registered post types to list.
		 *
		 * @since 1.3.0
		 */
		do_action('customizeposts_no_post_types_listing');
	}

	$taxonomies = customizeposts_get_taxonomy_data();
	echo '<h2 id="taxonomies">' . esc_html__('Taxonomies', 'customizeposts') . '</h2>';
	if (!empty($taxonomies)) {
		?>
				<p><?php printf(esc_html__('customizeposts registered taxonomies count total: %d', 'customizeposts'), count($taxonomies));?></p>

				<?php

		$taxonomy_table_heads = array(
			__('Taxonomy', 'customizeposts'),
			__('Settings', 'customizeposts'),
			__('Post Types', 'customizeposts'),
			__('Labels', 'customizeposts'),
			__('Template Hierarchy', 'customizeposts'),
		);

		/**
		 * Fires before the listing of registered taxonomy data.
		 *
		 * @since 1.1.0
		 */
		do_action('customizeposts_before_taxonomy_listing');
		?>
				<table class="wp-list-table widefat taxonomy-listing">
					<tr>
						<?php
foreach ($taxonomy_table_heads as $head) {
			echo '<th>' . esc_html($head) . '</th>';
		}?>
					</tr>
					<?php
$counter = 1;
		foreach ($taxonomies as $taxonomy => $taxonomy_settings) {

			$rowclass = (0 === $counter % 2) ? '' : 'alternate';

			$strings = array();
			$object_types = array();
			foreach ($taxonomy_settings as $settings_key => $settings_value) {
				if ('labels' === $settings_key) {
					continue;
				}

				if (is_string($settings_value)) {
					$strings[$settings_key] = $settings_value;
				} else {
					if ('object_types' === $settings_key) {
						$object_types[$settings_key] = $settings_value;

						// In case they are not associated from the post type settings.
						if (empty($object_types['object_types'])) {
							$types = get_taxonomy($taxonomy);
							$object_types['object_types'] = $types->object_type;
						}
					}
				}
			}
			?>
							<tr class="<?php echo esc_attr($rowclass); ?>">
								<?php
$edit_path = 'admin.php?page=customizeposts_manage_taxonomies&action=edit&customizeposts_taxonomy=' . $taxonomy;
			$taxonomy_link_url = (is_network_admin()) ? network_admin_url($edit_path) : admin_url($edit_path);?>
								<td>
									<?php printf(
				'<a href="%s">%s</a> | <a href="%s">%s</a>',
				esc_attr($taxonomy_link_url),
				sprintf(
					esc_html__('Edit %s', 'customizeposts'),
					esc_html($taxonomy)
				),
				esc_attr(admin_url('admin.php?page=customizeposts_orderreorder&action=get_code#' . $taxonomy)),
				esc_html__('Get code', 'customizeposts')
			);?>
								</td>
								<td>
									<?php
foreach ($strings as $key => $value) {
				printf('<strong>%s:</strong> ', esc_html($key));
				if (in_array($value, array('1', '0'))) {
					echo esc_html(disp_boolean($value));
				} else {
					echo esc_html($value);
				}
				echo '<br/>';
			}?>
								</td>
								<td>
									<?php
if (!empty($object_types['object_types'])) {
				foreach ($object_types['object_types'] as $type) {
					echo esc_html($type) . '<br/>';
				}
			}?>
								</td>
								<td>
									<?php
$maybe_empty = array_filter($taxonomy_settings['labels']);
			if (!empty($maybe_empty)) {
				foreach ($taxonomy_settings['labels'] as $key => $value) {
					printf(
						'%s: %s<br/>',
						esc_html($key),
						esc_html($value)
					);
				}
			} else {
				esc_html_e('No custom labels to display', 'customizeposts');
			}
			?>
								</td>
								<td>
									<p><strong><?php esc_html_e('Archives', 'customizeposts');?></strong><br />
										taxonomy-<?php echo esc_html($taxonomy); ?>-term_slug.php *<br />
										taxonomy-<?php echo esc_html($taxonomy); ?>.php<br />
										taxonomy.php<br />
										archive.php<br />
										index.php
									</p>

									<p>
										<?php esc_html_e('*Replace "term_slug" with the slug of the actual taxonomy term.', 'customizeposts');?>
									</p>
									<p><?php
printf(
				'<a href="https://developer.wordpress.org/themes/basics/template-hierarchy/">%s</a>',
				esc_html__('Template hierarchy Theme Handbook', 'customizeposts')
			);?></p>
								</td>
							</tr>

						<?php
$counter++;
		}
		?>
					<tr>
						<?php
foreach ($taxonomy_table_heads as $head) {
			echo '<th>' . esc_html($head) . '</th>';
		}?>
					</tr>
				</table>
			<?php
/**
		 * Fires after the listing of registered taxonomy data.
		 *
		 * @since 1.3.0
		 */
		do_action('customizeposts_after_taxonomy_listing');

	} else {

		/**
		 * Fires when there are no registered taxonomies to list.
		 *
		 * @since 1.3.0
		 */
		do_action('customizeposts_no_taxonomies_listing');
	}
	?>

		</div>
	<?php
}

/**
 * Displays a message for when no post types are registered.
 *
 * Uses the `customizeposts_no_post_types_listing` hook.
 *
 * @since 1.3.0
 *
 * @internal
 */
function customizeposts_no_post_types_to_list() {
	echo '<p>' . sprintf(esc_html__('No post types registered for display. Visit %s to get started.', 'customizeposts'),
		sprintf('<a href="%s">%s</a>',
			esc_attr(admin_url('admin.php?page=customizeposts_manage_post_types')),
			esc_html__('Add/Edit Post Types', 'customizeposts')
		)
	) . '</p>';
}
add_action('customizeposts_no_post_types_listing', 'customizeposts_no_post_types_to_list');

/**
 * Displays a message for when no taxonomies are registered.
 *
 * Uses the `customizeposts_no_taxonomies_listing` hook.
 *
 * @since 1.3.0
 *
 * @internal
 */
function customizeposts_no_taxonomies_to_list() {
	echo '<p>' . sprintf(esc_html__('No taxonomies registered for display. Visit %s to get started.', 'customizeposts'),
		sprintf('<a href="%s">%s</a>',
			esc_attr(admin_url('admin.php?page=customizeposts_manage_taxonomies')),
			esc_html__('Add/Edit Taxonomies', 'customizeposts')
		)
	) . '</p>';
}
add_action('customizeposts_no_taxonomies_listing', 'customizeposts_no_taxonomies_to_list');

<?php
/**
 * Customize Post Utility Code.
 *
 * @package customizeposts
 * @subpackage Utility
 * @author Citytechcorp.com
 * @since 1.0.0
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Edit links that appear on installed plugins list page, for our plugin.
 *
 * @since 1.0.0
 *
 * @internal
 *
 * @param array $links Array of links to display below our plugin listing.
 * @return array Amended array of links.
 */
function customizeposts_edit_plugin_list_links($links) {
	// We shouldn't encourage editing our plugin directly.
	unset($links['edit']);

	// Add our custom links to the returned array value.
	return array_merge(array(
		'<a href="' . admin_url('admin.php?page=customizeposts_main_menu') . '">' . __('About', 'customizeposts') . '</a>',
		'<a href="' . admin_url('admin.php?page=customizeposts_support') . '">' . __('Help', 'customizeposts') . '</a>',
	), $links);
}
add_filter('plugin_action_links_' . plugin_basename(dirname(dirname(__FILE__))) . '/customizeposts.php', 'customizeposts_edit_plugin_list_links');

/**
 * Returns SVG icon for custom menu icon
 *
 * @since 1.2.0
 *
 * @return string
 */
function customizeposts_menu_icon() {
	return 'dashicons-image-filter';
}

/**
 * Return boolean status depending on passed in value.
 *
 * @since 0.5.0
 *
 * @param mixed $bool_text text to compare to typical boolean values.
 * @return bool Which bool value the passed in value was.
 */
function get_disp_boolean($bool_text) {
	$bool_text = (string) $bool_text;
	if (empty($bool_text) || '0' === $bool_text || 'false' === $bool_text) {
		return false;
	}

	return true;
}

/**
 * Return string versions of boolean values.
 *
 * @since 0.1.0
 *
 * @param string $bool_text String boolean value.
 * @return string standardized boolean text.
 */
function disp_boolean($bool_text) {
	$bool_text = (string) $bool_text;
	if (empty($bool_text) || '0' === $bool_text || 'false' === $bool_text) {
		return 'false';
	}

	return 'true';
}

/**
 * Display footer links and plugin credits.
 *
 * @since 0.3.0
 *
 * @internal
 *
 * @param string $original Original footer content.
 * @return string $value HTML for footer.
 */
function customizeposts_footer($original = '') {

	$screen = get_current_screen();

	if (!is_object($screen) || 'customizeposts_main_menu' !== $screen->parent_base) {
		return $original;
	}

	return sprintf(
		__('%s version %s by %s', 'customizeposts'),
		__('CUSTOMIZE POST', 'customizeposts'),
		customizeposts_VERSION,
		'<a href="https://citytechcorp.com" target="_blank">Citytechcorp.com</a>'
	);
}
add_filter('admin_footer_text', 'customizeposts_footer');

/**
 * Conditionally flushes rewrite rules if we have reason to.
 *
 * @since 1.3.0
 */
function customizeposts_flush_rewrite_rules() {

	if (defined('DOING_AJAX') && DOING_AJAX) {
		return;
	}

	/*
		 * Wise men say that you should not do flush_rewrite_rules on init or admin_int. Due to the nature of our plugin
		 * and how new post types or taxonomies can suddenly be introduced, we need to...potentially. For this,
		 * we rely on a short lived transient. Only 5 minutes life span. If it exists, we do a soft flush before
		 * deleting the transient to prevent subsequent flushes. The only times the transient gets created, is if
		 * post types or taxonomies are created, updated, deleted, or imported. Any other time and this condition
		 * should not be met.
	*/
	if ('true' === ($flush_it = get_transient('customizeposts_flush_rewrite_rules'))) {
		flush_rewrite_rules(false);
		// So we only run this once.
		delete_transient('customizeposts_flush_rewrite_rules');
	}
}
add_action('admin_init', 'customizeposts_flush_rewrite_rules');

/**
 * Return the current action being done within customizeposts context.
 *
 * @since 1.3.0
 *
 * @return string Current action being done by customizeposts
 */
function customizeposts_get_current_action() {
	$current_action = '';
	if (!empty($_GET) && isset($_GET['action'])) {
		$current_action .= esc_textarea($_GET['action']);
	}

	return $current_action;
}

/**
 * Return an array of all post type slugs from Customize Post.
 *
 * @since 1.3.0
 *
 * @return array customizeposts post type slugs.
 */
function customizeposts_get_post_type_slugs() {
	$post_types = get_option('customizeposts_post_types');
	if (!empty($post_types)) {
		return array_keys($post_types);
	}
	return array();
}

/**
 * Return an array of all taxonomy slugs from Customize Post.
 *
 * @since 1.3.0
 *
 * @return array customizeposts taxonomy slugs.
 */
function customizeposts_get_taxonomy_slugs() {
	$taxonomies = get_option('customizeposts_taxonomies');
	if (!empty($taxonomies)) {
		return array_keys($taxonomies);
	}
	return array();
}

/**
 * Return the appropriate admin URL depending on our context.
 *
 * @since 1.3.0
 *
 * @param string $path URL path.
 * @return string|void
 */
function customizeposts_admin_url($path) {
	if (is_multisite() && is_network_admin()) {
		return network_admin_url($path);
	}

	return admin_url($path);
}

/**
 * Construct action tag for `<form>` tag.
 *
 * @since 1.3.0
 *
 * @param object|string $ui customizeposts Admin UI instance.
 * @return string
 */
function customizeposts_get_post_form_action($ui = '') {
	/**
	 * Filters the string to be used in an `action=""` attribute.
	 *
	 * @since 1.3.0
	 */
	return apply_filters('customizeposts_post_form_action', '', $ui);
}

/**
 * Display action tag for `<form>` tag.
 *
 * @since 1.3.0
 *
 * @param object $ui customizeposts Admin UI instance.
 */
function customizeposts_post_form_action($ui) {
	echo customizeposts_get_post_form_action($ui);
}

/**
 * Fetch our customizeposts post types option.
 *
 * @since 1.3.0
 *
 * @return mixed|void
 */
function customizeposts_get_post_type_data() {
	return apply_filters('customizeposts_get_post_type_data', get_option('customizeposts_post_types', array()), get_current_blog_id());
}

/**
 * Fetch our customizeposts taxonomies option.
 *
 * @since 1.3.0
 *
 * @return mixed|void
 */
function customizeposts_get_taxonomy_data() {
	return apply_filters('customizeposts_get_taxonomy_data', get_option('customizeposts_taxonomies', array()), get_current_blog_id());
}

/**
 * Checks if a post type is already registered.
 *
 * @since 1.3.0
 *
 * @param string       $slug Post type slug to check.
 * @param array|string $data Post type data being utilized.
 * @return mixed|void
 */
function customizeposts_get_post_type_exists($slug = '', $data = array()) {

	/**
	 * Filters the boolean value for if a post type exists for 3rd parties.
	 *
	 * @since 1.3.0
	 *
	 * @param string       $slug Post type slug to check.
	 * @param array|string $data Post type data being utilized.
	 */
	return apply_filters('customizeposts_get_post_type_exists', post_type_exists($slug), $data);
}

/**
 * Displays Citytechcorp.com products in a sidebar on the add/edit screens for post types and taxonomies.
 *
 * We hope you don't mind.
 *
 * @since 1.3.0
 *
 * @internal
 */
function customizeposts_products_sidebar() {

}
//add_action('customizeposts_below_post_type_tab_menu', 'customizeposts_products_sidebar');
//add_action('customizeposts_below_taxonomy_tab_menu', 'customizeposts_products_sidebar');

/**
 * Outputs our newsletter signup form.
 *
 * @since 1.3.4
 * @internal
 */
function customizeposts_newsletter_form() {

}

/**
 * Fetch all set ads to be displayed.
 *
 * @since 1.3.4
 *
 * @return array
 */
function customizeposts_get_ads() {

}

function customizeposts_admin_notices_helper($message = '', $success = true) {

	$class = array();
	$class[] = ($success) ? 'updated' : 'error';
	$class[] = 'notice is-dismissible';

	$messagewrapstart = '<div id="message" class="' . implode(' ', $class) . '"><p>';

	$messagewrapend = '</p></div>';

	$action = '';

	/**
	 * Filters the custom admin notice for customizeposts.
	 *
	 * @since 1.0.0
	 *
	 * @param string $value            Complete HTML output for notice.
	 * @param string $action           Action whose message is being generated.
	 * @param string $message          The message to be displayed.
	 * @param string $messagewrapstart Beginning wrap HTML.
	 * @param string $messagewrapend   Ending wrap HTML.
	 */
	return apply_filters('customizeposts_admin_notice', $messagewrapstart . $message . $messagewrapend, $action, $message, $messagewrapstart, $messagewrapend);
}

/**
 * Grab post type or taxonomy slug from $_POST global, if available.
 *
 * @since 1.0.0
 *
 * @internal
 *
 * @return string
 */
function customizeposts_get_object_from_post_global() {
	if (isset($_POST['cpt_custom_post_type']['name'])) {
		return sanitize_text_field($_POST['cpt_custom_post_type']['name']);
	}

	if (isset($_POST['cpt_custom_tax']['name'])) {
		return sanitize_text_field($_POST['cpt_custom_tax']['name']);
	}

	return esc_html__('Object', 'customizeposts');
}

/**
 * Successful add callback.
 *
 * @since 1.0.0
 */
function customizeposts_add_success_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has been successfully added', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		true
	);
}

/**
 * Fail to add callback.
 *
 * @since 1.0.0
 */
function customizeposts_add_fail_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has failed to be added', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		false
	);
}

/**
 * Successful update callback.
 *
 * @since 1.0.0
 */
function customizeposts_update_success_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has been successfully updated', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		true
	);
}

/**
 * Fail to update callback.
 *
 * @since 1.0.0
 */
function customizeposts_update_fail_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has failed to be updated', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		false
	);
}

/**
 * Successful delete callback.
 *
 * @since 1.0.0
 */
function customizeposts_delete_success_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has been successfully deleted', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		true
	);
}

/**
 * Fail to delete callback.
 *
 * @since 1.0.0
 */
function customizeposts_delete_fail_admin_notice() {
	echo customizeposts_admin_notices_helper(
		sprintf(
			esc_html__('%s has failed to be deleted', 'customizeposts'),
			customizeposts_get_object_from_post_global()
		),
		false
	);
}

/**
 * Returns error message for if trying to register existing post type.
 *
 * @since 1.0.0
 *
 * @return string
 */
function customizeposts_slug_matches_post_type() {
	return sprintf(
		esc_html__('Please choose a different post type name. %s is already registered.', 'customizeposts'),
		customizeposts_get_object_from_post_global()
	);
}

/**
 * Returns error message for if trying to register existing taxonomy.
 *
 * @since 1.0.0
 *
 * @return string
 */
function customizeposts_slug_matches_taxonomy() {
	return sprintf(
		esc_html__('Please choose a different taxonomy name. %s is already registered.', 'customizeposts'),
		customizeposts_get_object_from_post_global()
	);
}

/**
 * Returns error message for if trying to register post type with matching page slug.
 *
 * @since 1.0.0
 *
 * @return string
 */
function customizeposts_slug_matches_page() {
	return sprintf(
		esc_html__('Please choose a different post type name. %s matches an existing page slug, which can cause conflicts.', 'customizeposts'),
		customizeposts_get_object_from_post_global()
	);
}

/**
 * Returns error message for if trying to use quotes in slugs or rewrite slugs.
 *
 * @since 1.0.0
 *
 * @return string
 */
function customizeposts_slug_has_quotes() {
	return sprintf(
		esc_html__('Please do not use quotes in post type/taxonomy names or rewrite slugs', 'customizeposts'),
		customizeposts_get_object_from_post_global()
	);
}

/**
 * Error admin notice.
 *
 * @since 1.0.0
 */
function customizeposts_error_admin_notice() {
	echo customizeposts_admin_notices_helper(
		apply_filters('customizeposts_custom_error_message', ''),
		false
	);
}
